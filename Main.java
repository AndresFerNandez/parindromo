
import java.util.*;



public class Main {



    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        while (sc.hasNext()) {

            String s;



            s = sc.nextLine().replaceAll("[^\\dA-Za-z]", "").toLowerCase();

            char c[] = s.toCharArray();

            boolean par = false;

            boolean impar = false;

            for (int i = 1; i < (c.length) && ((!par || !impar)); i++) {

                if (c[i] == c[i - 1]) {

                    par = true;

                }

                if (i>1){

                if (c[i] == c[i - 2]) {

                    impar = true;

                }

                }

            }

            if (par && impar) {

                System.out.println("Ambos");

            } else if (par) {

                System.out.println("Par");

            } else if (impar) {

                System.out.println("Impar");

            } else {

                System.out.println("Ninguno");

            }



        }

    }

}
